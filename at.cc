// string::at
#include <iostream>
#include <string>

int main ()
{
    std::cout << "The character at the specified position in the string." << std::endl;
    std::cout << std::endl;
  std::string str ("Test string");
  std::cout << "This code prints out the content of a string character by character using the at member function:" << std::endl;
  for (unsigned i=0; i<str.length(); ++i)
  {
    std::cout << str.at(i);
  }
  std::cout << std::endl;
  return 0;
}
