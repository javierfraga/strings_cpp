// string::push_back
#include <iostream>
#include <fstream>
#include <string>

int main ()
{
    std::cout << "Appends character c to the end of the string, increasing its length by one." << std::endl;
    std::cout << "This example reads an entire file character by character, appending each character to a string object using push_back." << std::endl;
    std::cout << std::endl;
  std::string str;
  std::ifstream file ("./test-file",std::ios::binary);
  if (file) {
    while (!file.eof()) str.push_back(file.get());
  }
  std::cout << str << '\n';
  return 0;
}
