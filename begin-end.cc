// string::begin/end
#include <iostream>
#include <string>

int main ()
{
    std::string str ("Test string");
    for ( std::string::iterator it=str.begin(); it!=str.end(); ++it)
        std::cout << *it;
    std::cout << '\n';

    std::cout << str << std::endl;
    std::cout << "----" << std::endl;
    std::cout << std::endl;

    std::string s("Exemplar");
    std::cout << "Original string:" << s << std::endl;
    *s.begin() = 'e';
    *--s.end() = 'R';
    std::cout << s << std::endl;
    std::cout << "*--s.end() is needed to modify last element cause true last element must be \0?" << std::endl;
    std::cout << "*s.begin() to modify first element" << std::endl;
    std::cout << "I think is better to use back() and front() ops" << std::endl;

    auto i = s.cbegin();
    std::cout << *++i << '\n';
    //*i.begin() = 'e'; // error cbegin pointer cannot modify contents

    return 0;
}
