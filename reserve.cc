// string::reserve
#include <iostream>
#include <fstream>
#include <string>

int main ()
{
    std::cout << "This example reserves enough capacity in the string object to store an entire file, which is then read character by character. By reserving a capacity for the string of at least the size of the entire file, we try to avoid all the automatic reallocations that the object str could suffer each time that inserting a new character would make its length surpass its capacity." << std::endl;
    std::cout << std::endl;
    std::cout << "Also interesting see char size allocation for file in seekg or tellg, different than here. Which is better?" << std::endl;
    std::cout << std::endl;

  std::string str;

  std::ifstream file ("./test-file",std::ios::in|std::ios::ate);
  if (file) {
    std::ifstream::streampos filesize = file.tellg();
    str.reserve(filesize);

    file.seekg(0);
    while (!file.eof())
    {
      str += file.get();
    }
    std::cout << str;
  }
  return 0;
}
