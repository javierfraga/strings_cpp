// string::shrink_to_fit
#include <iostream>
#include <string>

int main ()
{
    std::cout << "Compare this to resize-length-capacity" << std::endl;
    std::cout << std::endl;
  std::string str (100,'x');
  std::cout << "1. capacity of str: " << str.capacity() << '\n';

  str.resize(10);
  std::cout << "2. capacity of str: " << str.capacity() << '\n';

  str.shrink_to_fit();
  std::cout << "3. capacity of str: " << str.capacity() << '\n';

  return 0;
}
