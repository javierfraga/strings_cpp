#include <iostream>
#include <string>

int main ()
{
  std::string str ("hello world!");
  std::cout << "Original String:" << str << std::endl;
  std::cout << std::endl;
  str.pop_back();
  std::cout << "After pop_back:" << str << std::endl;
  return 0;
}
