// string::data
#include <iostream>
#include <string>
#include <cstring>

int main ()
{
    std::cout << "Interesting return value of memcpy" << std::endl;
    std::cout << "Interesting way to compare two strings" << std::endl;
    std::cout << "I thought memcpy returned pointer here returns true/false" << std::endl;
    std::cout << std::endl;
    int length;
    std::string str = "Test string";
    char* cstr = (char*)"Test string";

    if ( str.length() == std::strlen(cstr) )
    {
        std::cout << "str and cstr have the same length.\n";
    }

    if ( (length = memcmp (cstr, str.data(), str.length() )) == 0 )
        std::cout << "str and cstr have the same content.\n";

    std::cout << "memcpy return value:" << length << std::endl;

    return 0;
}
