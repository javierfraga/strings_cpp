// string::operator[]
#include <iostream>
#include <string>

int main ()
{
    std::cout << "Array way to string type string, doesn't have to be char to use [] method" << std::endl;
    std::cout << std::endl;
  std::string str ("Test string");
  for (unsigned int i=0; i<str.length(); ++i)
  {
    std::cout << str[i];
  }
  std::cout << std::endl;
  return 0;
}
