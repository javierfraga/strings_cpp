// string::cbegin/cend
#include <iostream>
#include <string>

int main ()
{
  std::string str ("const cannot modify contents");
  for (auto it=str.cbegin(); it!=str.cend(); ++it){
    //*it = 'A'; // const cannot modify contents
    std::cout << *it;
  }
  std::cout << '\n';
  std::cout << "But we can increment pointer to string to access its elements" << std::endl;

  return 0;
}
