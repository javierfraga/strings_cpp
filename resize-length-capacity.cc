// resizing string
#include <iostream>
#include <string>

int main ()
{
  std::string str ("I like to code in C");
  std::cout << str << '\n';
  std::cout << "is length " << str.length() << std::endl;

  unsigned sz = str.size();

  str.resize (sz+2,'+');
  std::cout << str << '\n';

  str.resize (14);
  std::cout << str << '\n';

  str.resize (100);
  std::cout << str << '\n';
  std::cout << "is length " << str.length() << std::endl;
  std::cout << "is capacity " << str.capacity() << std::endl;
  return 0;
}
