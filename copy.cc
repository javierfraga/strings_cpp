// string::copy
#include <iostream>
#include <string>
#include <cstring>

int main ()
{
    std::cout << "Easier to use = assignment when copying string but " << std::endl;
    std::cout << "i guess compare allows you to choose the characters to copy" << std::endl;
    std::cout << "Difference between substr and copy is substr appends \0 where copy does not" << std::endl;
    std::cout << std::endl;
    char buffer[20];
    std::string str ("Test string...");
    std::string str1;
    std::size_t length = str.copy(buffer,6,5);
    str1 = str;
    buffer[length]='\0';
    std::cout << "buffer contains: " << buffer << '\n';
    std::cout << "str1 contains: " << str1 << '\n';
    if ( str.length() == str1.length() )
    {
        std::cout << "str and str1 have the same length.\n";
    }

    if ( str  == str1 )
        std::cout << "str and str1 have the same content with '=='.\n";

    if ( str.compare( str1 ) == 0 ) // 0 means they are equal
        std::cout << "str and str1 have the same content with 'compare()'.\n";

    return 0;
}
