// string::length
#include <iostream>
#include <string>

int main ()
{
  std::string str ("Test string");
  std::cout << "The length of str *" << str << "* is " << str.length() << " characters.\n";
  return 0;
}
