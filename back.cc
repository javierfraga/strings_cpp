// string::back
#include <iostream>
#include <string>

int main ()
{
    std::cout << "best way to modify last elemnent in string" << std::endl;
    std::cout << std::endl;
  std::string str ("hello world.");
  str.back() = '!';
  std::cout << str << '\n';
  return 0;
}
