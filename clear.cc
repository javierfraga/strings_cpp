// string::clear
#include <iostream>
#include <string>

int main ()
{
    std::cout << "Clears buffer char string" << std::endl;
    std::cout << std::endl;
  char c;
  std::string str;
  std::cout << "Please type some lines of text. Enter a dot (.) to finish:\n";
  do {
    c = std::cin.get();
    str += c;
    if (c=='\n')
    {
       std::cout << str;
       str.clear();
       std::cout << "Is string empty()?:" << str.empty() << std::endl;
    }
  } while (c!='.');
  return 0;
}
