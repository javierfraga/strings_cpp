// string::substr
#include <iostream>
#include <string>

int main ()
{
    std::cout << "Difference between substr and copy is substr appends \0 where copy does not" << std::endl;
    std::cout << std::endl;
  std::string str="We think in generalities, but we live in details.";
                                           // (quoting Alfred N. Whitehead)

  std::string str2 = str.substr (3,5);     // "think"

  std::size_t pos = str.find("live");      // position of "live" in str

  std::string str3 = str.substr (pos);     // get from "live" to the end

  std::cout << str2 << ' ' << str3 << '\n';

  return 0;
}
