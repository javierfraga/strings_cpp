// string::front
#include <iostream>
#include <string>

int main ()
{
    std::cout << "best way to modify first elemnent in string" << std::endl;
    std::cout << std::endl;
    std::string str ("test string");
    str.front() = 'T';
    std::cout << str << '\n';
    return 0;
}
