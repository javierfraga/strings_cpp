// comparing size, length, capacity and max_size
#include <iostream>
#include <string>

int main ()
{
  std::string str ("Test string");
  std::cout << "String *" << str << "* size: " << str.size() << "\n";
  std::cout << "String *" << str << "* length: " << str.length() << "\n";
  std::cout << "String *" << str << "* capacity: " << str.capacity() << "\n";
  std::cout << "String *" << str << "* max_size: " << str.max_size() << "\n";
  return 0;
}
