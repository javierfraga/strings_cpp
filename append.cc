// appending to string
#include <iostream>
#include <string>
using namespace std;

void print( string &str , string &str2 , string &str3 )
{
  std::cout << "str  :" << str  << std::endl;
  std::cout << "str2  :" << str2  << std::endl;
  std::cout << "str3  :" << str3  << std::endl;
  std::cout << std::endl;
}
int main ()
{
  std::string str;
  std::string str2="Writing ";
  std::string str3="print 10 and then 5 more";
  std::cout << "__Original strings__" << std::endl;
  print( str , str2 , str3 );

  // used in the same order as described above:
  str.append(str2);                       // "Writing "
  std::cout << "str.append(str2)" << std::endl;
  print( str , str2 , str3 );

  str.append(str3,6,3);                   // "10 "
  std::cout << "str.append(str3,6,3)" << std::endl;
  print( str , str2 , str3 );

  str.append("dots are cool",5);          // "dots "
  std::cout << "str.append(\"dots are cool\",5)" << std::endl;
  print( str , str2 , str3 );

  str.append("here: ");                   // "here: "
  std::cout << "str.append(\"here: \")" << std::endl;
  print( str , str2 , str3 );

  str.append(10u,'.');                    // ".........."
  std::cout << "str.append(10u,'.')" << std::endl;
  print( str , str2 , str3 );

  str.append(str3.begin()+8,str3.end());  // " and then 5 more"
  std::cout << "str.append(str3.begin()+8,str3.end())" << std::endl;
  print( str , str2 , str3 );

  str.append<int>(5,0x2E);                // "....."
  std::cout << "str.append<int>(5,0x2E)" << std::endl;
  print( str , str2 , str3 );

  return 0;
}
